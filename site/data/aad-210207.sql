-- MySQL dump 10.9
--
-- Host: localhost    Database: bagel
-- ------------------------------------------------------
-- Server version	4.1.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesslevels`
--

DROP TABLE IF EXISTS `accesslevels`;
CREATE TABLE `accesslevels` (
  `levelid` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(32) default NULL,
  PRIMARY KEY  (`levelid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accesslevels`
--


/*!40000 ALTER TABLE `accesslevels` DISABLE KEYS */;
LOCK TABLES `accesslevels` WRITE;
INSERT INTO `accesslevels` VALUES (2,'admin'),(1,'guest');
UNLOCK TABLES;
/*!40000 ALTER TABLE `accesslevels` ENABLE KEYS */;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menuid` int(10) unsigned NOT NULL auto_increment,
  `action` varchar(64) default NULL,
  `text` varchar(255) default NULL,
  `priority` int(10) unsigned default NULL,
  `accesslevel` int(10) unsigned default NULL,
  PRIMARY KEY  (`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--


/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
LOCK TABLES `menu` WRITE;
INSERT INTO `menu` VALUES (1,'logout','Logout',99,2),(2,'default','Home',1,2),(3,'resource','Resources',25,2),(4,'passwd','Change Password',80,2),(5,'xml','Generate Project Page',65,2),(6,'project','Projects',22,2),(7,'people','People',24,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `peopleid` int(10) unsigned NOT NULL auto_increment,
  `first` varchar(64) default NULL,
  `last` varchar(64) default NULL,
  `email` varchar(255) default NULL,
  `location` varchar(255) default NULL,
  PRIMARY KEY  (`peopleid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `people`
--


/*!40000 ALTER TABLE `people` DISABLE KEYS */;
LOCK TABLES `people` WRITE;
INSERT INTO `people` VALUES (1,'Thomas','Cort','tcort@gentoo.org','Williamstown, VT, USA'),(5,'Ryan','Gibbons','gibbonsr@routedtechnologies.com','Fort Worth, Texas, USA'),(6,'Gregory','Shikhman','cornmander@cornmander.com','Detroit, MI, USA'),(7,'Christian','Heim','phreak@gentoo.org','Germany'),(8,'Marius','Mauch','genone@gentoo.org','Northern Germany'),(9,'Alexandre','Buisse','nattfodd@gentoo.org','France'),(10,'Stefan','Schweizer','genstef@gentoo.org','Germany'),(11,'Simon','Stelling','blubb@gentoo.org','Switzerland'),(12,'Andrey','Falko','ma3oxuct@gmail.com','New York, NY, USA'),(13,'Thomas','Kear','thomas.kear@gmail.com','New Zealand'),(14,'Chris','White','ChrisWhite@gentoo.org','CA, USA'),(15,'Stephen','Becker','geoman@gentoo.org','Blacksburg, VA, USA'),(16,'Steve','Dibb','beandog@gentoo.org','West Jordan, UT, USA'),(17,'Steven','Oliver','oliver.steven@gmail.com','Bluefield, WV, USA'),(18,'Jann - Ove','Risvik','jann.ove@risvik.ath.cx','Norway'),(19,'Scott','Jubenville','linux.scott@gmail.com','ON, Canada'),(20,'Caleb','Tennis','caleb@gentoo.org','Columbus, IN, USA'),(21,'Steev','Klimaszewski','steev@gentoo.org','Tulsa, OK, USA'),(22,'Richard','Fish','bigfish@asmallpond.org','Phoenix, AZ, USA'),(23,'Tom','Wesley','tom@tomaw.net','UK'),(24,'Thomas','Heinrichsdobler','dertyp@gmail.com','Germany'),(25,'Luis Francisco','Araujo','araujo@gentoo.org','Maracaibo, Zulia state, Venezuela'),(26,'Patrick','Lauer','patrick@gentoo.org','Aachen, Germany'),(27,'Joshua','Jackson','tsunam@gentoo.org','Reno, NV, USA'),(28,'Carl','Fongheiser','carlfongheiser@gmail.com','Iowa City, IA, USA'),(29,'Joshua','Nichols','nichoj@gentoo.org','Boston, MA, USA'),(30,'Ioannis','Aslanidis','deathwing00@gentoo.org','Tarragona, Spain'),(31,'Kurt','Hindenburg','kurt.hindenburg@gmail.com','Indianapolis, IN, USA'),(32,'Andrew','Gaffney','agaffney@gentoo.org','St. Louis, MO, USA'),(33,'Robin','Johnson','robbat2@gentoo.org','Burnaby, BC, Canada'),(34,'Ildar','Sagdejev','specious@gmail.com','Durham, NC, USA'),(35,'Mr.','Anonymous','Anonymous','Paris, France');
UNLOCK TABLES;
/*!40000 ALTER TABLE `people` ENABLE KEYS */;

--
-- Table structure for table `project_members`
--

DROP TABLE IF EXISTS `project_members`;
CREATE TABLE `project_members` (
  `userid` int(10) unsigned default NULL,
  `roleid` int(10) unsigned default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_members`
--


/*!40000 ALTER TABLE `project_members` DISABLE KEYS */;
LOCK TABLES `project_members` WRITE;
INSERT INTO `project_members` VALUES (1,1),(2,2),(3,2),(4,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `project_members` ENABLE KEYS */;

--
-- Table structure for table `project_specific_resources`
--

DROP TABLE IF EXISTS `project_specific_resources`;
CREATE TABLE `project_specific_resources` (
  `resourceid` int(10) unsigned default NULL,
  `projectid` int(10) unsigned default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_specific_resources`
--


/*!40000 ALTER TABLE `project_specific_resources` DISABLE KEYS */;
LOCK TABLES `project_specific_resources` WRITE;
INSERT INTO `project_specific_resources` VALUES (15,NULL),(16,NULL),(17,4),(18,4),(17,5),(18,5),(17,6),(18,6),(19,NULL),(20,7),(19,7),(21,NULL),(22,8),(23,9),(24,10),(25,11),(26,12),(27,4),(28,12),(29,12),(15,13),(30,NULL),(31,NULL),(32,NULL),(33,NULL),(34,NULL),(35,NULL),(36,NULL),(37,NULL),(38,NULL),(39,NULL),(40,NULL),(41,NULL),(42,NULL),(43,NULL),(44,7),(44,14),(45,NULL),(46,NULL),(47,NULL),(48,NULL),(49,NULL),(50,15),(51,17),(52,11),(53,17),(54,NULL),(55,NULL),(56,18),(57,19),(57,20),(57,21),(58,7),(58,12),(59,7),(59,12),(60,22),(60,23),(61,NULL),(62,12),(63,1),(63,24),(64,25),(64,12),(65,NULL),(66,NULL),(67,NULL),(68,NULL),(69,NULL),(70,NULL),(71,NULL),(72,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `project_specific_resources` ENABLE KEYS */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `projectid` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `url` text,
  PRIMARY KEY  (`projectid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--


/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
LOCK TABLES `projects` WRITE;
INSERT INTO `projects` VALUES (1,'alpha','http://alpha.gentoo.org'),(2,'amd64','http://amd64.gentoo.org'),(3,'user-relations','http://www.gentoo.org/proj/en/devrel/user-relations/'),(4,'portage','http://gentoo.org/proj/en/portage'),(5,'gentoolkit','http://www.gentoo.org/doc/en/gentoolkit.xml'),(6,'gentoo-stats','http://sources.gentoo.org/viewcvs.py/portage/gentoo-stats/'),(7,'mobile','http://www.gentoo.org/proj/en/metastructure/herds/herds.xml#doc_chap68'),(8,'kernel','http://www.gentoo.org/proj/en/kernel/index.xml'),(9,'mips','http://www.gentoo.org/proj/en/base/mips/'),(10,'perl','http://www.gentoo.org/proj/en/perl/'),(11,'common-lisp','http://www.gentoo.org/proj/en/metastructure/herds/herds.xml#doc_chap20'),(12,'misc','http://gentoo.org'),(13,'java','http://www.gentoo.org/proj/en/java/'),(14,'gentopia','https://gentopia.gentooexperimental.org/'),(15,'haskell','http://www.gentoo.org/proj/en/metastructure/herds/herds.xml#doc_chap49'),(16,'scheme','http://www.gentoo.org/proj/en/metastructure/herds/herds.xml#doc_chap100'),(17,'lang-misc','http://www.gentoo.org/proj/en/metastructure/herds/herds.xml#doc_chap58'),(18,'x86','http://www.gentoo.org/proj/en/base/x86/'),(19,'ppc','http://ppc.gentoo.org'),(20,'macos','http://www.gentoo.org/proj/en/gentoo-alt/macos/'),(21,'ppc64','http://ppc64.gentoo.org'),(22,'kde','http://www.gentoo.org/proj/en/desktop/kde/index.xml'),(23,'gdp','http://www.gentoo.org/proj/en/gdp/'),(24,'installer','http://www.gentoo.org/proj/en/releng/installer/'),(25,'app-backup','http://www.gentoo.org/proj/en/metastructure/herds/herds.xml#doc_chap10');
UNLOCK TABLES;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources` (
  `resourceid` int(10) unsigned NOT NULL auto_increment,
  `donorid` int(10) unsigned default NULL,
  `devid` int(10) unsigned default NULL,
  `quantity` int(10) unsigned default NULL,
  `resource` text,
  `purpose` text,
  `status` int(10) unsigned default NULL,
  `date_created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `comment` text,
  PRIMARY KEY  (`resourceid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resources`
--


/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
LOCK TABLES `resources` WRITE;
INSERT INTO `resources` VALUES (2,1,NULL,1,'D-Link DFE-670TXD 10/100Mbps PCMCIA Fast Ethernet Adapter','null',2,'2006-07-03 14:49:14','2006-07-03 14:49:14',NULL),(3,1,NULL,1,'CentreCOM 210TS AUI to 10baseT Transceiver',NULL,2,'2006-07-03 14:46:48','2006-07-03 14:46:48',NULL),(17,13,8,1,'PCI parallel-ATA controller card with 2+ channels (no RAID\r\nfunctionality needed)','Replacing existing components that are suspected to overheat/fail from time to time causing my home server to go offline for hours or days, resulting in disrupted workflows and corrupted data == lost time.',4,'2006-08-08 10:39:51','2006-08-09 20:12:31',NULL),(15,5,14,1,'Hosting / Shell Accounts (6mbit, unlimited throughput)','apache tomcat work',3,'2006-08-04 10:58:15','2006-08-11 10:02:48',NULL),(16,6,NULL,1,'Hosting / Shell Accounts on Gentoo/x86 (100mbit, 1000GB/mo transfer)','null',2,'2006-08-05 05:15:03','2006-08-05 05:15:03',NULL),(18,NULL,8,1,'AGP (or PCI) low-end, passively cooled graphics card (must work in a\r\nsocket-A mainboard)','Replacing existing components that are suspected to overheat/fail from time to time causing my home server to go offline for hours or days, resulting in disrupted workflows and corrupted data == lost time.',4,'2006-08-08 10:40:28','2006-08-16 07:13:41',NULL),(19,NULL,9,1,'GPS receiver usable without a PDA (laptop or standalone units are fine). It must be in the list of the gpsd supported devices.','The purpose is to join the mobile herd and help with testing/maintenance of GPS related software. According to steev, nobody in this herd is currently owning a GPS device, which makes it pretty much impossible to maintain those applications.',1,'2006-08-08 10:59:30','2006-08-08 11:32:38',NULL),(20,NULL,10,1,'GPS receiver usable without a PDA (laptop or standalone units are fine). It must be in the list of the gpsd supported devices.','The purpose is to join the mobile herd and help with testing/maintenance of GPS related software. According to steev, nobody in this herd is currently owning a GPS device, which makes it pretty much impossible to maintain those applications.',1,'2006-08-08 11:29:17','2006-08-08 11:32:48',NULL),(21,11,NULL,1,'1x Firewire/IEEE-1394 PCI-card with a VIA VT6306 chipset and 3 plugs','null',2,'2006-08-08 12:17:23','2006-08-08 12:17:23',NULL),(22,12,7,1,'A shell account on a single box with root access and permission to reboot when needed.','Kernel development, testing, and rollout along with other day to day remote development.',3,'2006-08-08 13:20:28','2006-08-12 06:03:15',NULL),(23,NULL,15,1,'SGI Octane system board with support for R12000 and R14000 CPU modules. SGI Part No: 030-1467-001','Keeping X-related packages up to date on Gentoo/MIPS.',1,'2006-08-09 21:18:00','2006-08-09 21:18:00',NULL),(24,19,14,1,'Book: Programming perl By Larry Wall, Tom Christiansen, Jon Orwant ISBN: 0-596-00027-8','improve perl knowledge to help with perl packages',3,'2006-08-09 21:49:09','2006-08-11 10:03:12',NULL),(25,NULL,14,1,'Book: ANSI Common LISP ; ISBN 0133708756','improve lisp knowledge to help with lisp packages',1,'2006-08-09 21:52:16','2006-08-19 08:02:15',NULL),(26,NULL,14,1,'Book: GNU Autoconf, Automake, and Libtool ; ISBN 1578701902','improve auto-tool knowledge to help with the maintenance of many packages',4,'2006-08-09 21:54:51','2006-08-31 06:27:25',NULL),(51,NULL,25,1,'Book: Smalltalk, Objects, and Design ; ISBN 1583484906','I maintain all of our existing Smalltalk implementations packages (around 5 at the moment). I would like to improve our support for them and extend it including new packages (and maybe even creating a herd if possible). This book will be ideal to help me doing this.',1,'2006-08-19 08:09:45','2006-08-19 08:18:40',NULL),(27,NULL,14,1,'Book: Dive Into Python ; ISBN 1590593561','improve python skills to help understand portage code',1,'2006-08-09 21:56:13','2006-08-19 08:02:59',NULL),(28,28,14,1,'Dell Inspiron 4000 256MB memory stick','general development work, improve productivity',4,'2006-08-09 21:57:56','2006-08-23 14:35:01',NULL),(29,22,14,2,'PC2700 333mhz 512MB RAM','general development work, improve productivity',3,'2006-08-09 21:58:30','2006-08-23 16:16:11',NULL),(30,16,NULL,1,'SIIG USB 2.0 Dual-Port PCI Host Adapter','null',2,'2006-08-10 05:33:19','2006-08-10 05:33:19',NULL),(31,16,NULL,1,'ATI USB RF Remote','null',2,'2006-08-10 05:33:37','2006-08-10 05:33:37',NULL),(32,17,NULL,1,'Conexant F1156I/R2F PCI Modem (RS56-PCI Chipset)','null',2,'2006-08-10 09:59:26','2006-08-10 09:59:26',NULL),(33,17,NULL,1,'Linksys LNE100TX ver. 5.1 PCI EtherFast 10/100 LAN Card','null',2,'2006-08-10 09:59:54','2006-08-10 09:59:54',NULL),(34,17,NULL,1,'Single stick of 512M 133Mhz RAM (possibly made by Memorex)','null',2,'2006-08-10 10:00:49','2006-08-10 10:00:49',NULL),(35,17,NULL,1,'Black 3 1/2 inch floppy drive from a Dell. No faceplate.','null',2,'2006-08-10 10:01:59','2006-08-10 10:02:53',NULL),(36,17,NULL,1,'White 3 1/2 inch floppy drive.','null',2,'2006-08-10 10:03:29','2006-08-10 10:03:29',NULL),(37,5,NULL,1,'Xen Virtual Server, Unlimited storage within reason, 100MB+ Ram, 100Mbit Burstable, 5Mbit dedicated, unlimited throughput. Backup and Restore points can be created by request.','null',2,'2006-08-10 10:13:03','2006-08-16 13:15:05',NULL),(38,18,NULL,1,'Shell accounts on a 6/6mbit line with unlimited transfer.','null',2,'2006-08-10 14:12:26','2006-08-10 14:12:26',NULL),(39,20,NULL,1,'Book: OReilly MySQL reference manual ; ISBN 636920002659','null',4,'2006-08-11 10:04:42','2006-10-23 20:05:24',NULL),(40,20,NULL,1,'Book: Serial Communications in C++ ; ISBN 1558281983','null',2,'2006-08-11 10:05:04','2006-08-19 08:04:10',NULL),(41,20,NULL,1,'Book: Effective C++, 2nd Ed ; ISBN 0201924889','null',4,'2006-08-11 10:05:38','2006-10-23 20:05:27',NULL),(42,20,NULL,1,'Book: Programming Embedded Systems in C/C++ ; ISBN 1565923545','null',4,'2006-08-11 10:05:56','2006-09-06 06:21:35',NULL),(43,20,NULL,1,'Book: Programming Perl ; ISBN 0596000278','null',2,'2006-08-11 10:06:32','2006-08-19 08:04:31',NULL),(44,22,21,1,'Seagate 5400 100GB 2.5\" PATA HD','Upgrade a desktop machine which currently has a 20GB HD or a laptop which currently has a 10GB HD.',3,'2006-08-13 18:22:51','2006-08-19 13:41:27',NULL),(45,22,NULL,1,'Hitachi HTS721010G9SA00 100GB, 7200rpm, 2.5\" SATA HD.','null',2,'2006-08-13 18:25:04','2006-08-13 18:25:04',NULL),(46,22,NULL,1,'Hitachi HTS541080G9SA00, 80GB, 5400rpm, 2.5\" SATA HD.','null',2,'2006-08-13 18:25:15','2006-08-13 18:25:15',NULL),(47,16,NULL,1,'TDK 4800B IDE 52x CD-RW internal drive (beige)','null',2,'2006-08-14 05:35:12','2006-08-14 05:35:12',NULL),(48,23,NULL,1,'Sharp Zaurus SL-5500, dock and charger.','null',2,'2006-08-15 11:11:20','2006-08-15 11:11:20',NULL),(49,24,NULL,1,'Belkin Nostromo n50 Speedpad. For supporting [url=http://sf.net/projects/nostromodriver]nostromo driver[/url] ([url=http://bugs.gentoo.org/show_bug.cgi?id=80623]Bug #80623[/url]).','null',2,'2006-08-18 19:56:32','2006-08-19 10:57:10',NULL),(50,NULL,25,1,'Book: Types and Programming Languages ; ISBN 0262162091','This book will help me understand better the design of type systems in different programming languages. I am mainly requesting this book because i maintain Haskell (and belong to that herd too) packages which highly would benefit from the knowledge i can gain from it.',1,'2006-08-19 07:59:29','2006-08-19 08:03:28',NULL),(52,NULL,25,1,'Book: ANSI Common Lisp ; ISBN 0133708756','We got a bunch of Common Lisp packages in our tree. Nevertheless, the Lisp herd is very understaffed. I would like to give some help with them. This book will give me the necessary background to improve my Lisp knowledge so i can cooperate in a more accurate manner.',1,'2006-08-19 08:11:39','2006-08-19 08:11:39',NULL),(53,NULL,25,1,'Book: Dylan Programming: An Object Oriented and Dynamic Language ; ISBN 0201479761','Dylan is a very powerful object oriented language. I included support for this language inside Gentoo, adding one of its most famous compilers available inside the tree. This book would give me the necessary knowledge to continue enhancing and extending such a support.',1,'2006-08-19 08:13:50','2006-08-19 08:13:50',NULL),(54,26,NULL,1,'Access to gentooexperimental.org AMD64 server, useable for webhosting and other services','null',2,'2006-08-19 13:15:14','2006-08-19 13:16:22',NULL),(55,26,NULL,1,'Access to dev.gentooexperimental.org AMD64 server, uncapped 100Mbit connection, 250G diskspace, compile machine','null',2,'2006-08-19 13:15:51','2006-08-19 13:15:51',NULL),(56,NULL,27,1,'A scanner that is supported by the sane backend. [url=http://www.sane-project.org/sane-supported-devices.html]SANE - Supported Devices List[/url].','As no member of the x86 team has a scanner, it makes it hard to test the packages for stabilization. The maintainer of packages that require a scanner might not have a x86 box that is stable.',1,'2006-08-20 05:57:04','2006-08-20 05:59:04',NULL),(57,NULL,29,1,'Any ethernet card that is known to work in Linux and Mac OS X on a PowerMac G5.','To do testing of Java and other packages on ppc, ppc64, and maybe even ppc-macos.',4,'2006-08-31 14:46:51','2006-09-04 20:19:10',NULL),(58,NULL,21,1,'orinoco gold usb wireless device','To help with the drivers.',1,'2006-08-31 16:05:21','2006-08-31 16:05:37',NULL),(59,NULL,21,1,'Battery for a Japanese Sharp Mebius PC-CB1-CD to replace a dead battery (model number CE-BN12).','So that Steev isn\'t stuck to a power outlet.',1,'2006-08-31 16:07:56','2006-08-31 16:08:12',NULL),(60,NULL,30,1,'A bluetooth USB dongle from this [url=http://www.holtmann.org/linux/bluetooth/features.html]list[/url]','Replacement of a usb dongle, required to perform development and tests on kdebluetooth and other bluetooth-related applications running on Gentoo. Also required to keep an up-to-date bluetooth guide.',4,'2006-09-02 14:41:14','2006-09-08 07:46:03',NULL),(61,31,NULL,1,'Book: GNU Autoconf, Automake, and Libtool ; ISBN 1578701902','null',4,'2006-09-02 14:46:44','2006-10-23 20:05:21',NULL),(62,NULL,14,1,'[url=http://www.pricegrabber.com/search_getprod.php/masterid=535580/search=intel+pentium+4+2.0+ghz/]Intel Pentium 4 Northwood - 2.0A GHz Processor[/url] (2.0GHz, 512KB, 400 MHz, Socket 478 - MPN: BX80532PC2000D)','general development work, improve productivity',1,'2006-09-02 17:33:26','2006-09-02 17:33:26',NULL),(63,NULL,32,1,'Any linux-supported DEC Alpha workstation with at least a 400MHz processor, >=128MB of memory, a supported PCI video card, and a CD-ROM (bonus if it can read CD-RW without a problem).','Support for the alpha architecture in the Gentoo Installer.',1,'2006-09-04 15:28:45','2006-09-04 15:29:05',NULL),(64,NULL,33,1,'A PCI-E SCSI controller (needs to be U160 or better and and not wider than PCI-E x4; must have an external connector to use with tape drives) or a SCSI->iSCSI converter','To maintain iSCSI packages and to maintain a lot of the backup packages dealing with tape drives.',1,'2006-09-08 08:06:11','2006-09-08 08:06:50',NULL),(65,34,NULL,1,'External 8X DVD-ROM / 32X CD-ROM drive, connects via PCMCIA card (included)','null',2,'2006-09-19 16:43:51','2006-09-19 16:43:51',NULL),(66,34,NULL,1,'SpeedStream DSL/Cable router (one LAN port only)','null',2,'2006-09-19 16:44:16','2006-09-19 16:44:16',NULL),(67,35,NULL,3,'Pentium III 800/256/133/1.7V - Slot 1 CPU','null',2,'2006-10-17 03:48:33','2006-10-17 03:52:51',NULL),(68,35,NULL,1,'Pentium III 866/256/133/1.7V - Slot 1 CPU','null',2,'2006-10-17 03:49:04','2006-10-17 03:53:12',NULL),(69,35,NULL,1,'Pentium III 800/256/133/1.7V - Socket 370 CPU','null',2,'2006-10-17 03:49:41','2006-10-17 03:49:41',NULL),(70,35,NULL,2,'Pentium III 733/256/133/1.7V - Socket 370 CPU','null',2,'2006-10-17 03:50:18','2006-10-17 03:50:18',NULL),(71,35,NULL,10,'128MB Memory DIMM SDRAM *ECC* (Various brands)','null',2,'2006-10-17 03:50:34','2006-10-17 03:50:34',NULL),(72,NULL,14,1,'10GB+ SCSI Disk','I\'d currently like to use the alpha workstation I have for desktop related testing for the alpha herd, however I have a small drive (about 3 gigs) which could work for the basics, but I\'d like to do a bit more testing than that.',1,'2006-10-17 03:51:35','2006-10-17 03:51:35',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `roleid` int(10) unsigned NOT NULL auto_increment,
  `role` varchar(64) default NULL,
  PRIMARY KEY  (`roleid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--


/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
LOCK TABLES `roles` WRITE;
INSERT INTO `roles` VALUES (1,'Lead'),(2,'Member');
UNLOCK TABLES;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `sessionid` varchar(32) default NULL,
  `userid` int(10) unsigned NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--


/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
LOCK TABLES `sessions` WRITE;
INSERT INTO `sessions` VALUES ('2792b9b2ed7f3bb339c7bfb6010bda54',5),('l3ogg0m6b3eppb8o1atv5qi740',2),('5kmueilnr17d796mdgq23vjtn0',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `statusid` int(10) unsigned NOT NULL auto_increment,
  `status` varchar(255) default NULL,
  PRIMARY KEY  (`statusid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--


/*!40000 ALTER TABLE `status` DISABLE KEYS */;
LOCK TABLES `status` WRITE;
INSERT INTO `status` VALUES (1,'seeking'),(2,'offering'),(4,'hidden'),(3,'thanks');
UNLOCK TABLES;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userid` int(10) unsigned NOT NULL auto_increment,
  `first` varchar(64) default NULL,
  `last` varchar(64) default NULL,
  `username` varchar(64) default NULL,
  `passwd` varchar(32) default NULL,
  `accesslevel` int(10) unsigned default NULL,
  PRIMARY KEY  (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--


/*!40000 ALTER TABLE `users` DISABLE KEYS */;
LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'Thomas','Cort','tcort','449884d825dc41836cbdf026892220c6',2),(2,'Christel','Dahlskjær','christel','d6d38c300f94b81760281c2f6a370ffa',2),(4,'Joshua','Jackson','tsunam','159e683b42c950c710c74844a719f474',2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

