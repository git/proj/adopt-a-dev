<?php
# CATS Online Registration System
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class actor {
  var $name;
  var $priority; # integer from 0-99 (0 is the most urgent / first done)

  function actor($name,$priority) {
    $this->name = $name;
    $this->priority = $priority;
  }

  function set_priority($priority) {
    $this->priority = $priority;
  }

  function get_priority() {
    return $this->priority;
  }

  function execute() {
    return;
  }

  function set_name($name) {
    $this->name = $name;
  }

  function get_name() {
    return $this->name;
  }
}

?>
