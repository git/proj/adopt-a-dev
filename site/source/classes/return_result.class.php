<?php
# CATS Online Registration System
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class return_result {
  var $success;
  var $messages;

  function return_result($suc) {
    $this->success = $suc;
    $this->messages = array();
  }

  # true if OK : false if failure
  function success() {
    return $this->success;
  }

  function set_result($result) {
    $this->success = $result;
  }

  function add_message($mes) {
    array_push($this->messages,$mes);
  }

  function get_messages() {
    if (count($this->messages) > 0) {
      return $this->messages;
    } else {
      $temp = array();
      array_push($temp,"");
      return $temp;
    }
  }
}
?>
