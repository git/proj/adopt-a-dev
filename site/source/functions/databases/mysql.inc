<?php

# CATS Online Registration System
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

function db_get_max_rows($rs)
{
    return mysql_num_rows($rs);
}

function db_fetch_array($rs)
{
    return mysql_fetch_array($rs);
}

function db_query($query)
{
    global $db_conn;
    db_connect();
    $result = mysql_query($query, $db_conn);
    return new DBResult($result,$query);
}

function db_exec($query)
{
    global $db_conn;
    db_connect();
    $result = mysql_query($query, $db_conn);
    if (mysql_affected_rows($db_conn) > 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function db_connect()
{
    global $db_conn, $db_name, $db_user, $db_pass, $db_host;
    if ($db_conn == NULL || $db_conn == FALSE)
    {
        $db_conn = mysql_connect($db_host, $db_user, $db_pass);
        mysql_select_db($db_name);
    }
}

function db_data_seek($rs, $pos)
{
  mysql_data_seek($rs, $pos);
}


function db_close()
{
    global $db_conn;
    mysql_close($db_conn);
}

?>
