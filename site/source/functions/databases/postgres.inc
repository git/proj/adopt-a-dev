<?php

# CATS Online Registration System
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

function db_get_max_rows($rs)
{
    return pg_num_rows($rs);
}

function db_fetch_array($rs)
{
    return pg_fetch_array($rs);
}

function db_query($query)
{
    global $db_conn;
    db_connect();
    #print "---------IN----------";
    #print "<p>$query<p>";
    $result = pg_query($db_conn, $query);
    #print "<p>$query<p>";
    #print "----------OUT---------";
    return new DBResult($result,$query);
}

function db_exec($query)
{
    global $db_conn;
    db_connect();
    #print "*********IN**********";
    #print "<p>$query<p>";
    $result = pg_query($db_conn, $query);
    #print "*********OUT*********";
    if ($result == FALSE)
        print "<p>db_exec() failed, query: $query<p>";
    if (pg_affected_rows($result) > 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function db_connect()
{
    global $db_conn, $db_name, $db_user, $db_pass;
    if ($db_conn == NULL || $db_conn == FALSE)
    {
        $db_conn = pg_connect("dbname=$db_name user=$db_user");

        # For debugging purposes only
        #print "<p>Formed a new connection<p>";
    }
}

function db_data_seek($rs, $pos)
{
  @pg_fetch_row($rs, $pos);
}

function db_close()
{
    global $db_conn;
    pg_close($db_conn);
}

?>
