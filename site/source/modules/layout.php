<?php
# Adopt a Developer
#
# Copyright (C) 2004, 2005, 2006 Thomas Cort
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

$called_footers = 0;
$called_headers = 0;

class html_headers extends actor {
	function execute() {
		global $called_headers;

    $site_title = "Welcome to the Adopt a Developer Admin Page";
    $title_size = "6"; # range 1-7 (1 is the biggest)
	
    # html_headers is a singleton
		if (!$called_headers) {
			$called_headers = 1;
			
      # print some headers
			?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <title>
   <?php echo "$site_title"; ?>
  </title>
  <link href="style.css" rel="stylesheet" type="text/css">
 </head>
 <body>
  <!-- body table -->
  <table align="center" border="0" cellpadding="8" cellspacing="0" width="85%">
   <!-- top header -->
   <tr>
    <td valign="middle" width="25%" colspan="2">
     <!-- title table -->
     <table align="center" bgcolor="#000000" border="0" cellpadding="1" cellspacing="0" width="100%">
      <tr>
       <td valign="middle">
        <table bgcolor="#ffffff" border="0" cellpadding="8" cellspacing="0" width="100%">
         <tr>
          <td align="center" valign="middle">
           <a href="./?a=default"><?php echo "$site_title"; ?></a>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </table>
     <!-- end title table -->
    </td>
   </tr>
   <!-- end top header -->

   <tr>

    <!-- right column -->
    <td valign="top" width="75%">
     <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
       <td valign="top" width="100%">

			<?php
		}
		return new return_result(true);
  }
}

class html_footers extends actor {
  function execute() {
    global $called_footers, $username;
    $copyright = "&copy; 2004, 2005, 2006 Thomas Cort."
    . "<br>&copy; 2004 Thomas Cort, Patrick McLean, Scott Stoddard, Adam Beaumont.";

    # make html_footers a singleton
    if (!$called_footers) {
      $called_footers = 1;
      echo "<br>";

?>
       </td>
      </tr>
     </table>
    </td>
    <!-- end right column -->

    <!-- left column -->
    <td valign="top" width="25%">


    <?php if ($username == "guest") { ?>

     <!-- sidebox -->
     <table bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
       <td>
        <table border="0" cellpadding="3" cellspacing="1" width="100%">
         <tr>
          <td bgcolor="#cccccc">
           &nbsp;<span class="title">Login</span>
          </td>
         </tr>
         <tr>
          <td bgcolor="#ffffff">
           <?php trigger("display_login");
           ?>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </table>
     <!-- end sidebox -->

     <br>

     <?php } else { ?>

     <table bgcolor="#000000" border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
       <td>
        <table border="0" cellpadding="3" cellspacing="1" width="100%">
         <tr>
          <td bgcolor="#cccccc">
           &nbsp;<span class="title">Menu</span>
          </td>
         </tr>
         <tr>
          <td bgcolor="#ffffff">
           <?php trigger("display_menu");
           ?>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </table>


     <?php } ?>
    </td>
    <!-- end left column -->

   </tr>

   <tr>
    <td align="center" colspan="2">
     <!-- footer -->
       <span class="footer">
       <?php echo $copyright; ?>
       </span>
     <!-- end footer -->
    </td>
   </tr>
  </table>
  <!-- end body table -->
 </body>
</html>

<?php
    }
    return new return_result(true);
  }
}

class begin_story extends actor{
  function execute() {
  ?>
        <!-- story -->
        <table align="center" bgcolor="#000000" border="0" cellPadding="0" cellSpacing="0" width="100%">
         <tr>
          <td>
           <table border="0" cellpadding="8" cellspacing="1" width="100%">
            <tr>
             <td bgcolor="#ffffff" colspan="2">
              <table border="0" cellpadding="4" cellspacing="0" width="100%">
               <tr>
                <td valign="top" width="100%">
    <?php
    return new return_result(true);
  }
}

class end_story extends actor{
  function execute() {
  ?>
                </td>
               </tr>
              </table>
             </td>
            </tr>
           </table>
          </td>
         </tr>
        </table>
        <!-- end story -->

    <?php
    return new return_result(true);
  }
}

class menu_event extends actor {
  function execute() {
    global $accesslevel;

    # get the menu items for the user's access level
    $db_result = db_query("select * from menu,accesslevels where accesslevels.levelid = '$accesslevel' and accesslevels.levelid = menu.accesslevel order by menu.priority;");

    echo "<ul class=\"navbar\">";

    # print them all out
    while ($db_result->has_next()) {
      $row = $db_result->get_row();
      echo "<li><a href=\"./?a=$row[2]\"><b>$row[3]</b></a></li>";
    }
	
    echo "</ul>";
	
    return new return_result(true);
  }
}

class search_action extends actor {
  function execute() {
    global $accesslevel, $username;
    trigger("html_headers");
    trigger("display_search");
    trigger("html_footers");
    return new return_result(true);
  }
}

class display_search_event extends actor {
  function execute() {

    echo "<br><div align=\"center\"><form action=\"./\" method=\"post\"><table>";
    echo "<tr><td colspan=\"3\">";
    echo "<input type=\"text\" name=\"query\"></td></tr><tr><td>";
    echo "<input type=\"hidden\" name=\"a\" value=\"default\">";
    echo "<input type=\"hidden\" name=\"search\" value=\"1\">";
    echo "&nbsp;</td><td><input type=\"submit\" id=\"button\" style=\"font-size:8pt;background-color:#cccccc;color:#000000\" value=\"search\">";
    echo "</td><td><input type=\"reset\" id=\"button\" style=\"font-size:8pt;background-color:#cccccc;color:#000000\" value=\"clear\"></td></tr>";
    echo "</table>";
    echo "</form></div>";

    return new return_result(true);
  }
}

class blogroll_event extends actor {
  function execute() {
    $result = db_query("SELECT url,name FROM links WHERE UPPER(name) LIKE '%BLOG%' ORDER BY name");

    echo "<ul class=\"navbar\">";

    while($result->has_next()) {
      $row = $result->get_row();
      $row[1] = preg_split("/\'/",$row[1]);
      echo "<li><a href=\"$row[0]\"><b>".$row[1][0]."</b></a></li>";
    }

    echo "</ul>";
    return new return_result(true);
  }
}

register_handler(new blogroll_event("blogroll",50));
register_handler(new begin_story("begin_story",50));
register_handler(new end_story("end_story",50));
register_handler(new html_headers("html_headers",50));
register_handler(new html_footers("html_footers",50));
register_handler(new display_search_event("display_search",50));
register_action(new search_action("search",50));
register_handler(new menu_event("menu",50));

?>
