<?php
# Adopt a Developer
#
# Copyright (C) 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class resource_action extends actor {
  function execute() {
    trigger("html_headers");
    if (isset($_REQUEST['resource_add'])) {
      trigger("resource_add");
    }
    trigger("resource");
    trigger("html_footers");
    return new return_result(true);
  }
}

class resource_event extends actor {
  function status_select_list($select) {
    $result = db_query("SELECT statusid, status from status");
    echo '<select name="status">';
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }
  function people_select_list($select,$name) {
    $result = db_query("SELECT peopleid, last, first from people order by last");
    echo "<select name=\"$name\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1], $row[2]</option>\n";
    }
    echo "</select>";
  }

  function project_resource_select_list($resourceid) {
    $result = db_query("SELECT projects.projectid, name from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$resourceid'");
    echo "<select name=\"del_project\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function project_select_list() {
    $result = db_query("SELECT projectid, name from projects order by name");
    echo "<select name=\"add_project\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function project_exists($projectid) {
     $result = db_query("select count(*) from projects where projectid = '$projectid'");
     $temp = $result->get_row();
     return $temp[0];
  }
  function execute() {
    global $username, $accesslevel;

    if ($username == "guest") {
    trigger("begin_story");
?>
<h2>Permission Denied</h2>
   <?php 
     trigger("end_story");
    } else {

    trigger("begin_story");

    echo "<h2>Developers Seeking Resources</h2><table>";
    $result = db_query("SELECT resourceid, first, last, location, quantity, resource, purpose, status.status, date_created, date_modified, email from people, resources, status where status.statusid = resources.status and status.status = 'seeking' and people.peopleid = devid");
    while ($result->has_next()) {
        $row = $result->get_row();
        echo "<tr><th>ID</th><td bgcolor=\"#eeeeee\">$row[0]</td><th>Qnty</th><td bgcolor=\"#eeeeee\">$row[4]</td><th>Status</th><td bgcolor=\"#eeeeee\">$row[7]</td></tr>";
        echo "<tr><th>Donor</th><td bgcolor=\"#eeeeee\">N/A</td><th>Resource</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[5]</td></tr>";
        echo "<tr><th>Dev</th><td bgcolor=\"#eeeeee\">$row[1] $row[2]</td><th>Purpose</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[6]</td></tr>";
        echo "<tr><th>Dev e-mail</th><td bgcolor=\"#eeeeee\"><a href=\"mailto:$row[10]\">$row[10]</a></td><th>Donor e-mail</th><td bgcolor=\"#eeeeee\">N/A</td><th>Edit</th><td bgcolor=\"#eeeeee\"><a href=\"./?a=edit&amp;id=$row[0]\">Click Here</a></td></tr>";

        echo "<tr><th>Project</th><td bgcolor=\"#eeeeee\">";
        $subresult = db_query("SELECT name,url from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$row[0]'");
        while ($subresult->has_next()) {
          $subrow = $subresult->get_row();
          echo "<a href=\"$subrow[1]\">$subrow[0]</a> ";
        }
        echo "<th>Created</th><td bgcolor=\"#eeeeee\">$row[8]</td><th>Modified</th><td bgcolor=\"#eeeeee\">$row[9]</td></tr>";
        if ($result->has_next()) {
          echo "<tr><td color=\"#ffffff\">&nbsp;</td><td color=\"#ffffff\">&nbsp;</td></tr>";
        }
    }
    echo "</table>";

    echo "<h2>Community Members Offering Resources</h2><table>";
    $result = db_query("SELECT resourceid, first, last, location, quantity, resource, status.status, date_created, date_modified, email from people, resources, status where status.statusid = resources.status and status.status = 'offering' and people.peopleid = donorid");
    while ($result->has_next()) {
        $row = $result->get_row();
        echo "<tr><th>ID</th><td bgcolor=\"#eeeeee\">$row[0]</td><th>Qnty</th><td bgcolor=\"#eeeeee\">$row[4]</td><th>Status</th><td bgcolor=\"#eeeeee\">$row[6]</td></tr>";
        echo "<tr><th>Donor</th><td bgcolor=\"#eeeeee\">$row[1] $row[2]</td><th>Resource</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[5]</td></tr>";
        echo "<tr><th>Dev</th><td bgcolor=\"#eeeeee\">N/A</td><th>Purpose</th><td colspan=\"3\" bgcolor=\"#eeeeee\">N/A</td></tr>";
        echo "<tr><th>Dev e-mail</th><td bgcolor=\"#eeeeee\">N/A</td><th>Donor e-mail</th><td bgcolor=\"#eeeeee\"><a href=\"mailto:$row[9]\">$row[9]</a></td><th>Edit</th><td bgcolor=\"#eeeeee\"><a href=\"./?a=edit&amp;id=$row[0]\">Click Here</a></td></tr>";
        echo "<tr><th>Project</th><td bgcolor=\"#eeeeee\">N/A</td><th>Created</th><td bgcolor=\"#eeeeee\">$row[7]</td><th>Modified</th><td  bgcolor=\"#eeeeee\">$row[8]</td></tr>";

        if ($result->has_next()) {
          echo "<tr><td color=\"#ffffff\">&nbsp;</td><td color=\"#ffffff\">&nbsp;</td></tr>";
        }
    }
    echo "</table>";

    echo "<h2>Completed Donations</h2><table>";

    $result = db_query("SELECT resourceid, donor.first, donor.last, dev.first, dev.last, quantity, resource, purpose, status.status as status, date_created, date_modified, donor.email, dev.email from people as donor, people as dev, resources, status where dev.peopleid = devid and donor.peopleid = donorid and status.status = 'thanks' and status.statusid = resources.status order by resourceid");
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<tr><th>ID</th><td bgcolor=\"#eeeeee\">$row[0]</td><th>Qnty</th><td bgcolor=\"#eeeeee\">$row[5]</td><th>Status</th><td bgcolor=\"#eeeeee\">$row[8]</td></tr>";
      echo "<tr><th>Donor</th><td bgcolor=\"#eeeeee\">$row[1] $row[2]</td><th>Resource</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[6]</td></tr>";
      echo "<tr><th>Dev</th><td bgcolor=\"#eeeeee\">$row[3] $row[4]</td><th>Purpose</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[7]</td></tr>";
      echo "<tr><th>Dev e-mail</th><td bgcolor=\"#eeeeee\"><a href=\"mailto:$row[11]\">$row[11]</a></td><th>Donor e-mail</th><td bgcolor=\"#eeeeee\"><a href=\"mailto:$row[12]\">$row[12]</a></td><th>Edit</th><td bgcolor=\"#eeeeee\"><a href=\"./?a=edit&amp;id=$row[0]\">Click Here</a></td></tr>";
      echo "<tr><th>Project</th><td bgcolor=\"#eeeeee\">";

      $subresult = db_query("SELECT name,url from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$row[0]'");
      while ($subresult->has_next()) {
        $subrow = $subresult->get_row();
        echo "<a href=\"$subrow[1]\">$subrow[0]</a> ";
      }
      echo "</td><th>Created</th><td bgcolor=\"#eeeeee\">$row[9]</td><th>Modified</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[10]</td></tr>";

      if ($result->has_next()) {
        echo "<tr><td color=\"#ffffff\">&nbsp;</td><td color=\"#ffffff\">&nbsp;</td></tr>";
      }
    }

    echo "</table>";
    echo "<h2>Hidden Resources</h2><table>";
    $result = db_query("SELECT resourceid, quantity, purpose, resource, date_created, date_modified from resources, status where status.statusid = resources.status and status.status = 'hidden'");
    while ($result->has_next()) {
      $row = $result->get_row();
      $subresult = db_query("select donorid, devid from resources where resourceid = '$row[0]'");
      $subrow = $subresult->get_row();
      echo "<tr><th>ID</th><td bgcolor=\"#eeeeee\">$row[0]</td><th>Qnty</th><td bgcolor=\"#eeeeee\">$row[1]</td><th>Status</th><td bgcolor=\"#eeeeee\">hidden</td></tr>";

      if ($subrow[0]) {
        $subresult = db_query("select first, last, email from people where peopleid = '$subrow[0]'");
        $don = $subresult->get_row();
        $donor = "$don[0] $don[1]";
        $donor_email = "$don[2]";
      } else {
        $donor = "NULL";
        $donor_email = "NULL";
      }

      if ($subrow[1]) {
        $subresult = db_query("select first, last, email from people where peopleid = '$subrow[1]'");
        $don = $subresult->get_row();
        $dev = "$don[0] $don[1]";
        $dev_email = "$don[2]";
      } else {
        $dev = "NULL";
        $dev_email = "NULL";
      }

      echo "<tr><th>Donor</th><td bgcolor=\"#eeeeee\">$donor</td><th>Resource</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[3]</td></tr>";
      echo "<tr><th>Dev</th><td bgcolor=\"#eeeeee\">$dev</td><th>Purpose</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[2]</td></tr>";
      echo "<tr><th>Dev e-mail</th><td bgcolor=\"#eeeeee\"><a href=\"mailto:$dev_email\">$dev_email</a></td><th>Donor e-mail</th><td bgcolor=\"#eeeeee\"><a href=\"mailto:$donor_email\">$donor_email</a></td><th>Edit</th><td bgcolor=\"#eeeeee\"><a href=\"./?a=edit&amp;id=$row[0]\">Click Here</a></td></tr>";
      echo "<tr><th>Project</th><td bgcolor=\"#eeeeee\">";

      $subresult = db_query("SELECT name,url from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$row[0]'");
      while ($subresult->has_next()) {
        $subrow = $subresult->get_row();
        echo "<a href=\"$subrow[1]\">$subrow[0]</a> ";
      }
      echo "</td><th>Created</th><td bgcolor=\"#eeeeee\">$row[4]</td><th>Modified</th><td colspan=\"3\" bgcolor=\"#eeeeee\">$row[5]</td></tr>";

      if ($result->has_next()) {
        echo "<tr><td color=\"#ffffff\">&nbsp;</td><td color=\"#ffffff\">&nbsp;</td></tr>";
      }
    }
    echo "</table>";
?>      <h2>Add a Resource</h2>
      <form action="./" method="post">
      <table>
      <tr><th>ID</th><td bgcolor="#eeeeee">N/A</td><th>Qnty</th><td bgcolor="#eeeeee"><input type="text" name="qnty" value="1" size="2"></td><th>Status</th><td bgcolor="#eeeeee"><?php $this->status_select_list(1); ?></td></tr>
      <tr><th>Donor</th><td bgcolor="#eeeeee"><?php $this->people_select_list(0,"donor"); ?></td><th>Resource</th><td colspan="3" bgcolor="#eeeeee"><input type="text" name="resource"></td></tr>
      <tr><th>Dev</th><td bgcolor="#eeeeee"><?php $this->people_select_list(0,"dev"); ?></td><th>Purpose</th><td colspan="3" bgcolor="#eeeeee"><input type="text" name="purpose"></td></tr>
      <tr><th>Add Project</th><td bgcolor="#eeeeee"><?php $this->project_select_list(); ?></td></th>
      <th>Submit</th><td bgcolor="#eeeeee">
      <input type="hidden" name="resource_add" value="1">
      <input type="hidden" name="a" value="resource"> <input type="hidden" name="commit" value="yes">
      <INPUT type="submit" value="Add"></td><th>Clear</th><td bgcolor="#eeeeee"><INPUT type="reset"></td></tr>
      </table>
      </form>
<?php


    trigger("end_story");
    }
    return new return_result(true);
  }
}

class resource_add_event extends actor {
  function status_select_list($select) {
    $result = db_query("SELECT statusid, status from status");
    echo '<select name="status">';
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function people_select_list($select,$name) {
    $result = db_query("SELECT peopleid, last, first from people order by last");
    echo "<select name=\"$name\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1], $row[2]</option>\n";
    }
    echo "</select>";
  }

  function project_resource_select_list($resourceid) {
    $result = db_query("SELECT projects.projectid, name from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$resourceid'");
    echo "<select name=\"del_project\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function project_select_list() {
    $result = db_query("SELECT projectid, name from projects order by name");
    echo "<select name=\"add_project\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function project_exists($projectid) {
     $result = db_query("select count(*) from projects where projectid = '$projectid'");
     $temp = $result->get_row();
     return $temp[0];
  }



  function execute() {
    trigger("begin_story");
          $fail_msg = "";
          $qnty = (isset($_REQUEST['qnty']) && is_numeric($_REQUEST['qnty']) && $_REQUEST['qnty'] > 0) ? $_REQUEST['qnty'] : "null";

          if ($qnty == "null") {
            $fail_msg .= "Quantity must be > 0<br>";
          }

          $resource = (isset($_REQUEST['resource']) && $_REQUEST['resource'] != "") ? $_REQUEST['resource'] : "null";
          if ($resource == "null") {
            $fail_msg .= "Resource must not be empty<br>";
          }

          $statusid = (isset($_REQUEST['status']) && is_numeric($_REQUEST['status'])) ? $_REQUEST['status'] : "null";
          if ($statusid != "null") {
            $result = db_query("select status from status where statusid = '$statusid'");
            if (!$result->has_next()) {
              $fail_msg .= "Status not found<br>";
            } else {
              $myrow = $result->get_row();
              $status = $myrow[0];
            }
          } else {
              $fail_msg .= "Status ID must be numeric<br>";
          }

          $purpose = (isset($_REQUEST['purpose']) && $_REQUEST['purpose'] != "") ? $_REQUEST['purpose'] : "null";
          if ($purpose == "null" && $status != "offering" && $status != "hidden") {
            $fail_msg .= "Purpose must not be empty<br>";
          }

          $devid = (isset($_REQUEST['dev']) && is_numeric($_REQUEST['dev'])) ? $_REQUEST['dev'] : "null";
          if ($status != "offering" && $status != "hidden") {
            if ($devid == "null") {
              $fail_msg .= "Dev ID must be numeric<br>";
            } else {
              $result = db_query("select * from people where peopleid = '$devid'");
              if (!$result->has_next()) {
                $fail_msg .= "Developer not found<br>";
              }
            }
          }

          $donorid = (isset($_REQUEST['donor']) && is_numeric($_REQUEST['donor'])) ? $_REQUEST['donor'] : "null";
          if ($status != "seeking" && $status != "hidden") {
            if ($donorid == "null") {
              $fail_msg .= "Donor ID must be numeric<br>";
            } else {
              $result = db_query("select * from people where peopleid = '$donorid'");
              if (!$result->has_next()) {
                $fail_msg .= "Donor not found<br>";
              }
            }
          }

          $add_project = (isset($_REQUEST['add_project']) && (is_numeric($_REQUEST['add_project']) || $_REQUEST['add_project'] == "NULL")) ? $_REQUEST['add_project'] : "null";

          if ($add_project == "null") {
            $fail_msg .= "Add project must give a numeric id or NULL";
          }

          if (is_numeric($add_project) && $this->project_exists($add_project) == 0) {
            $fail_msg .= "Add project: invalid project id";
          }

          if ($fail_msg == "") {
            $sql  = "insert into resources ";
            $sql .= "(quantity,status,";
            if (is_numeric($donorid)) {
              $sql .= "donorid,";
            }
            if (is_numeric($devid)) {
              $sql .= "devid,";
            }
            $sql .= "resource,purpose,date_created,date_modified) values (";
            $sql .= "$qnty,$statusid,";
            if (is_numeric($donorid)) {
              $sql .= "$donorid,";
            }
            if (is_numeric($devid)) {
              $sql .= "$devid,";
            }
            $sql .= "'".doslashes($resource) . "','" . doslashes($purpose) ."',NOW(),NOW())";
            if (db_exec($sql)) {
              $result = db_query("select resourceid from resources where resource = '".doslashes($resource)."' and purpose = '".doslashes($purpose)."' and status = $statusid");
              $row = $result->get_row();
              $resourceid = $row[0];
              db_exec("insert into project_specific_resources (resourceid,projectid) values ($resourceid,$add_project)");
              echo "Done";
            } else {
              echo "sql failure: $sql";
            }
          } else {
            echo $fail_msg;
          }
          trigger("end_story");
          echo "<br>";
 return new return_result(true);
  }
}

register_handler(new resource_event("resource",50));
register_handler(new resource_add_event("resource_add",50));
register_action(new resource_action("resource",50));

?>
