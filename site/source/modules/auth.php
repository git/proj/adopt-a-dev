<?php
# Adopt a Developer
#
# Copyright (C) 2004, 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class login_action extends actor {
  function execute() {
    trigger("html_headers");
    if (isset($_REQUEST['login'])) {
      trigger("login");
    } else {
      trigger("display_login");
    }
    trigger("html_footers");
    return new return_result(true);
  }
}

class display_login_event extends actor {
  function execute() {
    global $username;

    if ($username == "guest") {

      echo "<br><div align=\"center\"><form action=\"./\" method=\"post\"><table>";
      echo "<tr><td><small>Username: </small></td><td>";
      echo "<input type=\"text\" name=\"username\" size=\"10\"></td></tr><tr><td><small>Password: </small></td><td>";
      echo "<input type=\"password\" name=\"passwd\" size=\"10\"></td></tr><tr><td align=\"center\">";
      echo "<input type=\"hidden\" name=\"a\" value=\"login\">";
      echo "<input type=\"hidden\" name=\"login\" value=\"1\">";
      echo "<input type=\"submit\" id=\"button\" style=\"font-size:8pt;background-color:#cccccc;color:#000000\" value=\"login\">";
      echo "</td><td align=\"center\"><input type=\"reset\" id=\"button\" style=\"font-size:8pt;background-color:#cccccc;color:#000000\" value=\"clear\"></td></tr>";
      echo "</table>";
      echo "</form></div>";

    } else {

          echo "<h2>Already Logged In</h2>";
          echo "<h2>Thank You! Come again!</h2>";

    }

    return new return_result(true);    
  }
}

class login_event extends actor {
  function execute() {
    global $username, $accesslevel;

    $_username = isset($_REQUEST['username']) ? doslashes($_REQUEST['username']) : "";
    $password  = isset($_REQUEST['passwd'])   ? md5($_REQUEST['passwd']): "";
    $sessionid = session_id();

    if ($_username != "" and $password !="") {
      $result = db_query("SELECT users.userid,users.accesslevel FROM users WHERE username='$_username' and passwd='$password';");
      if ($result->has_next()) {
        $row    = $result->get_row();
        $userid = $row[0];
        $_accesslevel = $row[1];
        $result = db_query("SELECT * FROM sessions WHERE userid = '$userid';");

        if ($result->has_next() && !db_exec("DELETE FROM sessions WHERE userid = '$userid';")) {
          trigger("begin_story");
          echo "<h2>Database Error</h2>";
          echo "<h2>Thank You! Come again!</h2>";
          trigger("end_story");
        }

        if (db_exec("INSERT INTO sessions (sessionid,userid) VALUES ('$sessionid','$userid');")) {
          $username = $_username;
          $accesslevel = $_accesslevel;
          trigger("default");
        } else {
          trigger("begin_story");
          echo "<h2>Database Error</h2>";
          echo "<h2>Thank You! Come again!</h2>";
          trigger("end_story");
        }
      } else {
        trigger("begin_story");
        echo "<h2>Authentication Error</h2>";
        echo "<h2>Thank You! Come again!</h2>";
        trigger("end_story");
      }
    } else {
      trigger("begin_story");
      echo "<h2>Authentication Error</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
	  }
    return new return_result(true);
  }
}

class logout_action extends actor {
  function execute() {
    global $accesslevel, $username;
    trigger("html_headers");

    $sessionid = session_id();
    if (db_exec("DELETE FROM sessions WHERE sessionid = '$sessionid';")) {
      session_unset();
      session_destroy();
      $accesslevel = 1;
      $username = "guest";
      trigger("begin_story");
      echo "<h2>Logged Out!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
      echo "<br \>";
      trigger("default");
    } else {
      trigger("begin_story");
      echo "<h2>Database Error!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
    }
    trigger("html_footers");
    return new return_result(true);
  }
}

class passwd_action extends actor {
  function execute() {
    global $accesslevel, $username;
    trigger("html_headers");
    if (isset($_REQUEST['passwd'])) {
      trigger("passwd");
    } else {
      trigger("display_passwd");
    }
    trigger("html_footers");
    return new return_result(true);
  }
}

class display_passwd_event extends actor {
  function execute() {
    global $username;

    trigger("begin_story");

    if ($username != "guest") {
      echo "<h2>Change Password</h2>";
      echo "<form action=\"./\" method=\"post\"><table>";
      echo "<tr><th>Old Password: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"password\" name=\"old_passwd\"></td></tr><tr><td>";
      echo "<tr><th>New Password: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"password\" name=\"new_passwd\"></td></tr><tr><td>";
      echo "<tr><th>Again: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"password\" name=\"again_passwd\"></td></tr><tr><td bgcolor=\"#eeeeee\">";
      echo "<input type=\"hidden\" name=\"a\" value=\"passwd\">";
      echo "<input type=\"hidden\" name=\"passwd\" value=\"1\">";
      echo "&nbsp;</td><td bgcolor=\"#eeeeee\"><input type=\"submit\" id=\"button\" value=\"change\">";
      echo "</td><td bgcolor=\"#eeeeee\"><input type=\"reset\" id=\"button\" value=\"clear\"></td></tr>";
      echo "</table></form>";
    } else {

      echo "<h2>You aren't Logged In!</h2>";
      echo "<h2>Thank You! Come again!</h2>";

    }

    trigger("end_story");
    return new return_result(true);
  }
}

class passwd_event extends actor {
  function execute() {
    global $username;

    $old_passwd   = isset($_REQUEST['old_passwd'])   ? md5($_REQUEST['old_passwd'])   : "";
    $again_passwd = isset($_REQUEST['again_passwd']) ? md5($_REQUEST['again_passwd']) : "";
    $new_passwd   = isset($_REQUEST['new_passwd'])   ? md5($_REQUEST['new_passwd'])   : "";

    if ($old_passwd != "" && $again_passwd != "" && $new_passwd != "") {
      if ($new_passwd == $again_passwd) {
        $result = db_query("SELECT * from users WHERE username = '$username' AND passwd = '$old_passwd';");
        if ($result->has_next()) {
          if (db_exec("UPDATE users SET passwd = '$new_passwd' WHERE username = '$username';")) {
            trigger("begin_story");
            echo "<h2>Password Updated!</h2>";
            trigger("end_story");
            echo "<br \>";
            trigger("default");
          } else {
            trigger("begin_story");
            echo "<h2>Database Error!</h2>";
            echo "<h2>Thank You! Come again!</h2>";
            trigger("end_story");
          }
        } else {
          trigger("begin_story");
          echo "<h2>Wrong Old Password!</h2>";
          echo "<h2>Thank You! Come again!</h2>";
          trigger("end_story");
        }
      } else {
        trigger("begin_story");
        echo "<h2>New and Again Passwords Don't Match!</h2>";
        echo "<h2>Thank You! Come again!</h2>";
        trigger("end_story");
      }
    } else {
      trigger("begin_story");
      echo "<h2>Incomplete Form Data!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
    }

    return new return_result(true);
  }
}

register_handler(new login_event("login",50));
register_handler(new display_login_event("display_login",50));
register_action(new login_action("login",50));
register_action(new logout_action("logout",50));
register_handler(new passwd_event("passwd",50));
register_handler(new display_passwd_event("display_passwd",50));
register_action(new passwd_action("passwd",50));
?>
