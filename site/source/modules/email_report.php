<?php
# Adopt a Developer
#
# Copyright (C) 2005, 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class email_report_action extends actor {
  function execute() {
global $guidexml;
header ("Content-type: text/plain");
$sessionid = session_id();

$accesslevel = 1;

$result = db_query("SELECT users.username,users.accesslevel FROM sessions,users WHERE sessions.sessionid = '$sessionid' AND users.userid = sessions.userid");
if ($result->has_next()) {
  $row = $result->get_row();
  $username = $row[0];
  $accesslevel = $row[1];
}

if ($accesslevel != 1) {

$guidexml = -1;
include_once("../functions/bbcode.php");


echo "\n\n";

echo "Community Member Offers\n";
echo "=======================\n\n";

$result = db_query("SELECT first, last, location, quantity, resource, date_modified from people, resources, status where status.statusid = resources.status and status.status = 'offering' and people.peopleid = donorid and UNIX_TIMESTAMP(date_modified) > UNIX_TIMESTAMP(now()) - 604800;");
while ($result->has_next()) {
  $row = $result->get_row();

  for ($i = 0; $i <= 5; $i++) {
    $uid = make_bbcode_uid();
    $row[$i] = bbencode_first_pass($row[$i],$uid);
    $row[$i] = bbencode_second_pass($row[$i],$uid);
  }


  echo "Offered Resource: $row[4]\n";
  echo "Name: $row[0] $row[1]\n";
  echo "Location: $row[2]\n";
  echo "Last Modified: $row[5]\n\n";
}

echo "\n\n";
echo "New Developer Requests\n";
echo "======================\n\n";


$result = db_query("SELECT first, last, location, quantity, resource, purpose, resourceid, date_modified from people, resources, status where status.statusid = resources.status and status.status = 'seeking' and people.peopleid = devid and UNIX_TIMESTAMP(date_modified) > UNIX_TIMESTAMP(now()) - 604800;");
while ($result->has_next()) {
  $row = $result->get_row();

  for ($i = 0; $i <= 5; $i++) {
    $uid = make_bbcode_uid();
    $row[$i] = bbencode_first_pass($row[$i],$uid);
    $row[$i] = bbencode_second_pass($row[$i],$uid);
  }

  echo "Resource: $row[4]\n";
  echo "Purpose: $row[5]\n";
  echo "Name: $row[0] $row[1] // Location: $row[2] // Last Modified: $row[7]\n\n";
}

}
return new return_result(true);
}}
register_action(new email_report_action("email_report",1));
?>
