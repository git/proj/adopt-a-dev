<?php
# Adopt a Developer
#
# Copyright (C) 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class edit_action extends actor {
  function execute() {
    trigger("html_headers");
    trigger("edit");
    trigger("html_footers");
    return new return_result(true);
  }
}

class edit_event extends actor {
  function status_select_list($select) {
    $result = db_query("SELECT statusid, status from status");
    echo '<select name="status">';
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function people_select_list($select,$name) {
    $result = db_query("SELECT peopleid, last, first from people order by last");
    echo "<select name=\"$name\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1], $row[2]</option>\n";
    }
    echo "</select>";
  }

  function project_resource_select_list($resourceid) {
    $result = db_query("SELECT projects.projectid, name from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$resourceid'");
    echo "<select name=\"del_project\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function project_select_list() {
    $result = db_query("SELECT projectid, name from projects order by name");
    echo "<select name=\"add_project\">";
    echo "<OPTION value=\"NULL\">NULL</OPTION>";
    while ($result->has_next()) {
      $row = $result->get_row();
      echo "<OPTION ";
      if ($row[0] == $select) {
        echo "selected ";
      }
      echo "value=\"$row[0]\">$row[1]</option>\n";
    }
    echo "</select>";
  }

  function project_exists($projectid) {
     $result = db_query("select count(*) from projects where projectid = '$projectid'");
     $temp = $result->get_row();
     return $temp[0];
  }

  function execute() {
    global $username, $accesslevel;

    if ($username == "guest") {
    trigger("begin_story");
?>
<h2>Permission Denied</h2>
    trigger("end_story");
   <?php } else {

    $resourceid = (isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : -1);

    trigger("begin_story");

    if ($resourceid == -1) {
      echo "<h2>Invalid Resource</h2>";
    } else {

      $result = db_query("select donorid, devid, quantity, resource, purpose, status from resources where resourceid = '$resourceid'");

      if (!$result->has_next()) {
        echo "<h2>Invalid Resource</h2>";
      } else {
        $row = $result->get_row();

        if (isset($_REQUEST['commit']) && $_REQUEST['commit'] = "yes") {
          $fail_msg = "";
          $qnty = (isset($_REQUEST['qnty']) && is_numeric($_REQUEST['qnty']) && $_REQUEST['qnty'] > 0) ? $_REQUEST['qnty'] : "null";

          if ($qnty == "null") {
            $fail_msg .= "Quantity must be > 0<br>";
          }

          $resource = (isset($_REQUEST['resource']) && $_REQUEST['resource'] != "") ? $_REQUEST['resource'] : "null";
          if ($resource == "null") {
            $fail_msg .= "Resource must not be empty<br>";
          }
          $resource = doslashes($resource);

          $statusid = (isset($_REQUEST['status']) && is_numeric($_REQUEST['status'])) ? $_REQUEST['status'] : "null";
          if ($statusid != "null") {
            $result = db_query("select status from status where statusid = '$statusid'");
            if (!$result->has_next()) {
              $fail_msg .= "Status not found<br>";
            } else {
              $myrow = $result->get_row();
              $status = $myrow[0];
            }
          } else {
              $fail_msg .= "Status ID must be numeric<br>"; 
          }

          $purpose = (isset($_REQUEST['purpose']) && $_REQUEST['purpose'] != "") ? $_REQUEST['purpose'] : "null";
          if ($purpose == "null" && $status != "offering" && $status != "hidden") {
            $fail_msg .= "Purpose must not be empty<br>";
          }
          $purpose = doslashes($purpose);

          $devid = (isset($_REQUEST['dev']) && is_numeric($_REQUEST['dev'])) ? $_REQUEST['dev'] : "null";
          if ($status != "offering" && $status != "hidden") {
            if ($devid == "null") {
              $fail_msg .= "Dev ID must be numeric<br>";
            } else {
              $result = db_query("select * from people where peopleid = '$devid'");
              if (!$result->has_next()) {
                $fail_msg .= "Developer not found<br>";
              }
            }
          }

          $donorid = (isset($_REQUEST['donor']) && is_numeric($_REQUEST['donor'])) ? $_REQUEST['donor'] : "null";
          if ($status != "seeking" && $status != "hidden") {
            if ($donorid == "null") {
              $fail_msg .= "Donor ID must be numeric<br>";
            } else {
              $result = db_query("select * from people where peopleid = '$donorid'");
              if (!$result->has_next()) {
                $fail_msg .= "Donor not found<br>";
              }
            }
          }

          $result = db_query("select count(*) from project_specific_resources where resourceid = '$resourceid'");
          $temp = $result->get_row();
          $num_projects = $temp[0];

          $add_project = (isset($_REQUEST['add_project']) && (is_numeric($_REQUEST['add_project']) || $_REQUEST['add_project'] == "NULL")) ? $_REQUEST['add_project'] : "null";
          $del_project = (isset($_REQUEST['del_project']) && (is_numeric($_REQUEST['del_project']) || $_REQUEST['del_project'] == "NULL")) ? $_REQUEST['del_project'] : "null";

          if ($add_project == "null") {
            $fail_msg .= "Add project must give a numeric id or NULL";
          }

          if ($del_project == "null") {
            $fail_msg .= "Del project must give a numeric id or NULL";
          }

          if (is_numeric($add_project) && $this->project_exists($add_project) == 0) {
            $fail_msg .= "Add project: invalid project id";
          }

          if (is_numeric($del_project) && $this->project_exists($del_project) == 0) {
            $fail_msg .= "Del project: invalid project id";
          }

          if (is_numeric($add_project)) {
            $result = db_query("select count(*) from project_specific_resources where resourceid = '$resourceid' and projectid = '$add_project'");
            $temp = $result->get_row();
            if ($temp[0] > 0) {
              $fail_msg .= "The project you are trying to add is already associated with this resource";
            }
          }

          if ($num_projects == 1 && !is_numeric($add_project) && is_numeric($del_project)) {
            $fail_msg .= "A resource must have at least 1 project associated with it";
          }

          if ($fail_msg == "") {
            $sql  = "update resources set ";
            $sql .= "quantity = '$qnty', status = '$statusid', ";
            if (is_numeric($donorid)) {
              $sql .= "donorid = '$donorid', ";
            } else {
              $sql .= "donorid = NULL, ";
            }
            if (is_numeric($devid)) {
              $sql .= "devid = '$devid', ";
            } else {
              $sql .= "devid = NULL, ";
            }
            $sql .= "resource = '" . doslashes($resource) . "', purpose = '" . doslashes($purpose) ."', ";
            $sql .= "date_created = date_created, ";
            $sql .= "date_modified = NOW() ";
            $sql .= "where resourceid = $resourceid";
            if (db_exec($sql)) {
              if (is_numeric($add_project)) {
                db_exec("insert into project_specific_resources (resourceid,projectid) values ($resourceid,$add_project)");
              }
              if (is_numeric($del_project)) {
                db_exec("delete from project_specific_resources where resourceid = $resourceid and projectid = $del_project");
              }
              echo "Done";
            } else {
              echo "sql failure: $sql";
            }
          } else {
            echo $fail_msg;
          }
          trigger("end_story");
          echo "<br>";
          trigger("begin_story");
        }

      $result = db_query("select donorid, devid, quantity, resource, purpose, status from resources where resourceid = '$resourceid'");
      $row = $result->get_row();
?>
      <h2>Edit a Resource</h2>
      <form action="./" method="post">
      <table>
      <tr><th>ID</th><td bgcolor="#eeeeee"><?php echo $resourceid; ?></td><th>Qnty</th><td bgcolor="#eeeeee"><input type="text" name="qnty" value="<?php echo $row[2]; ?>" size="2"></td><th>Status</th><td bgcolor="#eeeeee"><?php $this->status_select_list($row[5]); ?></td></tr>
      <tr><th>Donor</th><td bgcolor="#eeeeee"><?php $this->people_select_list($row[0],"donor"); ?></td><th>Resource</th><td colspan="3" bgcolor="#eeeeee"><input type="text" name="resource" value="<?php echo $row[3]; ?>"></td></tr>
      <tr><th>Dev</th><td bgcolor="#eeeeee"><?php $this->people_select_list($row[1],"dev"); ?></td><th>Purpose</th><td colspan="3" bgcolor="#eeeeee"><input type="text" name="purpose" value="<?php echo $row[4]; ?>"></td></tr>
      <tr><th>Projects</th><td bgcolor="#eeeeee" colspan="5"><?php
      $result = db_query("SELECT name, url from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$resourceid'");
      while ($result->has_next()) {
        $row = $result->get_row();
        echo "<a href=\"$row[1]\">$row[0]</a> ";
      }
      ?>
      </td></tr>
      <tr><th>Add Project</th><td bgcolor="#eeeeee"><?php $this->project_select_list(); ?></td></th><th>Del Project</th><td bgcolor="#eeeeee" colspan="3"><?php $this->project_resource_select_list($resourceid); ?></td></th></tr>
      <tr><th>Submit</th><td bgcolor="#eeeeee">
      <input type="hidden" name="id" value="<?php echo $resourceid; ?>">
      <input type="hidden" name="a" value="edit"> <input type="hidden" name="commit" value="yes">
      <INPUT type="submit" value="Send"></td><th>Clear</th><td bgcolor="#eeeeee" colspan="3"><INPUT type="reset"></td></tr>
      </table>
      </form>
<?php
    }
   }
   trigger("end_story");
  }
   return new return_result(true);
 }
}

register_handler(new edit_event("edit",50));
register_action(new edit_action("edit",50));

?>
