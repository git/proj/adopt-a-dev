<?php
# Adopt a Developer
#
# Copyright (C) 2006 Thomas Cort
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# This event displays the menu on the screen.  Menu items are displayed
# appropriately depending on access level and priority.
#
# Output is simple and should be improved with the use of css in the future.
#
# The menu has no associated action (ie. it cannot be directly keyed-into.)
class display_menu_event extends actor {
  function execute() {
    global $accesslevel;

    # Get the menu items for the user's access level
    $db_result = db_query("select * from menu where accesslevel = '$accesslevel' order by priority;");

    echo "<p><ul>";

    # Print them all out
    while ($db_result->has_next()) {
      $row = $db_result->get_row();
      echo "<li><a href=\"./index.php?a=$row[1]\">$row[2]</a></li>";
    }

    echo "</ul></p>";

    return new return_result(true);
  }
}

register_handler(new display_menu_event("display_menu",50));
?>
