<?php
# Adopt a Developer
#
# Copyright (C) 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class project_action extends actor {
  function execute() {
    global $accesslevel, $username;
    trigger("html_headers");
    if (isset($_REQUEST['project'])) {
      trigger("project");
      trigger("display_project");
    } else if (isset($_REQUEST['project_add'])) {
      trigger("project_add");
      trigger("display_project");
    } else {
      trigger("display_project");
    }
    trigger("html_footers");
    return new return_result(true);
  }
}

class display_project_event extends actor {
  function execute() {

    trigger("begin_story");

    if ($username != "guest") {
      echo "<h2>Projects (ordered by name)</h2>";
      $result = db_query("select projectid, name, url from projects order by name");
      while ($result->has_next()) {
        $row = $result->get_row();
        echo "<form action=\"./\" method=\"post\"><table>";
        echo "<tr><th>Name: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
        echo "<input type=\"text\" name=\"project_name\" value=\"$row[1]\"></td>";
        echo "<th>URL: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
        echo "<input type=\"text\" name=\"project_url\" value=\"$row[2]\"></td><td bgcolor=\"#eeeeee\">";
        echo "<input type=\"hidden\" name=\"a\" value=\"project\">";
        echo "<input type=\"hidden\" name=\"project\" value=\"1\">";
        echo "<input type=\"hidden\" name=\"projectid\" value=\"$row[0]\">";
        echo "&nbsp;</td><td bgcolor=\"#eeeeee\"><input type=\"submit\" id=\"button\" value=\"change\">";
        echo "</td><td bgcolor=\"#eeeeee\"><input type=\"reset\" id=\"button\" value=\"clear\"></td></tr>";
        echo "</table></form><br>";
      }

      echo "<h2>Add a Project</h2>";
      echo "<form action=\"./\" method=\"post\"><table>";
      echo "<tr><th>Project Name: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"text\" name=\"project_name\"></td></tr><tr><td>";
      echo "<tr><th>URL: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"text\" name=\"project_url\"></td></tr><tr><td bgcolor=\"#eeeeee\">";
      echo "<input type=\"hidden\" name=\"a\" value=\"project\">";
      echo "<input type=\"hidden\" name=\"project_add\" value=\"1\">";
      echo "&nbsp;</td><td bgcolor=\"#eeeeee\"><input type=\"submit\" id=\"button\" value=\"add\">";
      echo "</td><td bgcolor=\"#eeeeee\"><input type=\"reset\" id=\"button\" value=\"clear\"></td></tr>";
      echo "</table></form>";

    } else {

      echo "<h2>You aren't Logged In!</h2>";
      echo "<h2>Thank You! Come again!</h2>";

    }

    trigger("end_story");
    return new return_result(true);
  }
}

class project_event extends actor {
  function execute() {

    $projectid    = isset($_REQUEST['projectid']) && is_numeric($_REQUEST['projectid']) ? $_REQUEST['projectid'] : "";
    $project_name = isset($_REQUEST['project_name']) ? $_REQUEST['project_name'] : "";
    $project_url  = isset($_REQUEST['project_url'])  ? $_REQUEST['project_url']   : "";

    if (is_numeric($projectid) && $project_url != "" && $project_name != "") {
      $result = db_query("SELECT * from projects where url = '".doslashes($project_url)."' and name = '".doslashes($project_name)."';");
      if (!$result->has_next()) {
        $result = db_query("SELECT * from projects where projectid = $projectid");
        if ($result->has_next()) {
          if (db_exec("update projects set url = '".doslashes($project_url)."', name = '".doslashes($project_name)."' where projectid = $projectid")) {
             trigger("begin_story");
             echo "<h2>Done!</h2>";
             trigger("end_story");
          } else {
             trigger("begin_story");
             echo "<h2>DB Error!</h2>";
             trigger("end_story");
          }
        } else {
          trigger("begin_story");
          echo "<h2>Project ID does not exist!</h2>";
          echo "<h2>Thank You! Come again!</h2>";
          trigger("end_story");
        }
      } else {
        trigger("begin_story");
        echo "<h2>Nothing Has Changed!</h2>";
        echo "<h2>Thank You! Come again!</h2>";
        trigger("end_story");
      }
    } else {
      trigger("begin_story");
      echo "<h2>Incomplete Form Data!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
    }

    echo "<br>";
    return new return_result(true);
  }
}

class project_add_event extends actor {
  function execute() {

    $project_name = isset($_REQUEST['project_name']) ? $_REQUEST['project_name'] : "";
    $project_url  = isset($_REQUEST['project_url'])  ? $_REQUEST['project_url']  : "";

    if ($project_url != "" && $project_name != "") {
      if (db_exec("insert into projects (name,url) values ('".doslashes($project_name)."','".doslashes($project_url)."')")) {
        trigger("begin_story");
        echo "<h2>Done!</h2>";
        trigger("end_story");
      } else {
        trigger("begin_story");
        echo "<h2>DB Error</h2>";
        trigger("end_story");
      }
    } else {
      trigger("begin_story");
      echo "<h2>Incomplete Form Data!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
    }

    echo "<br>";
    return new return_result(true);
  }
}

register_handler(new project_event("project",50));
register_handler(new project_add_event("project_add",50));
register_handler(new display_project_event("display_project",50));
register_action(new project_action("project",50));
?>
