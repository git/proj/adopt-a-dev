<?php
# Adopt a Developer
#
# Copyright (C) 2005, 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class xml_action extends actor {
  function execute() {
    global $guidexml;

header ("Content-type: text/plain");
$sessionid = session_id();

$accesslevel = 1;

$result = db_query("SELECT users.username,users.accesslevel FROM sessions,users WHERE sessions.sessionid = '$sessionid' AND users.userid = sessions.userid");
if ($result->has_next()) {
  $row = $result->get_row();
  $username = $row[0];
  $accesslevel = $row[1];
}

if ($accesslevel != 1) {

$guidexml = 1;
include_once("../functions/bbcode.php");


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
echo "<?xml-stylesheet href=\"/xsl/project.xsl\" type=\"text/xsl\"?>\n";
echo "<?xml-stylesheet href=\"/xsl/guide.xsl\" type=\"text/xsl\"?>\n";

?>
<!-- This page was automatically generated. DO NOT EDIT IT DIRECTLY! -->
<!-- $Header: $ -->

<!DOCTYPE project SYSTEM "http://www.gentoo.org/dtd/project.dtd">

<project>
<name>adopt-a-dev</name>

<longname>Adopt a Developer (beta)</longname>

<date><?php echo date("Y-m-d"); ?></date>

<author title="Author">
  <mail link="tcort@gentoo.org">Thomas Cort</mail>
</author>

<description>
  This project aims to connect developers who need resources 
  (ie hardware, technical books, shell accounts, etc) with people
  and companies from the community who want to donate resources.
</description>

<longdescription>
  This project aims to connect developers who need resources with people
  and companies from the community who want to donate resources. Useful
  resources include computer hardware, books, shell accounts, and
  anything else that enhances the development process. This project is
  not involved with monetary donations; if you wish to donate money,
  you may do so through <uri 
link="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&amp;business=paypal@gentoo.org&amp;item_name=Gentoo+Linux+Support&amp;item_number=1000&amp;image_url=/images/paypal.png&amp;no_shipping=1&amp;return=http://www.gentoo.org&amp;cancel_return=http://www.gentoo.org">paypal</uri>.
  If you are a developer looking for monetary resources, then you
  may be interested in the <uri link="/foundation/en/funds.xml">
  Gentoo Foundation Funding and Expenditures</uri> page.
  This project does not collect any resources itself, it only acts
  a central point of contact for potential donors and individual 
  developers.
</longdescription>

<?php
  # Get the list of project members
  $db_result = db_query("select username, role from users, roles, project_members  where users.userid = project_members.userid and roles.roleid = project_members.roleid ;");

  # Print them all out
  while ($db_result->has_next()) {
    $row = $db_result->get_row();
    echo "<dev role=\"$row[1]\">$row[0]</dev>\n";
  }
?>

<extrachapter>
  <title>Project Status</title>
  <section>
  <body>

  <p>
  This project is still in its infancy. It is in <c>beta</c>
  since we are still getting feedback and working out all of the 
  details. If you'd like to share your ideas about the project with us, 
  we may be contacted at
  <mail link="adopt-a-dev@gentoo.org">adopt-a-dev@gentoo.org</mail>. 
  While the project is in beta, we will only deal with items that can 
  be obtained for less than $250 USD.
  </p>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>The Need for Resources</title>
  <section>
  <body>

  <p>
  A lot of the packages in the tree require specific hardware to test.
  For example, cdrtools requires a CD burner, qc-usb requires a QuickCam,
  wireless-tools requires a wireless network card, etc. Donating items
  like those allows us to ensure they work on the many architectures we
  support. Hardware donations can also be used to replace broken parts;
  we all know what it's like to lose a hard drive or some other crucial
  component. It is especially hard on developers who are students on a
  budget. We are always looking to support new architectures or
  architectures that have little developer help. Donating rare and exotic
  hardware can improve such situations.
  </p>

  <p>
  Computer hardware isn't the only thing that a developer might find 
  useful. Gentoo developers can also benefit greatly from technical 
  books. Books can enhance a developer's ability to solve bugs and 
  maintain ebuilds. Books can also aid in the development of Gentoo 
  related software projects like <uri link="/proj/en/portage">portage</uri>.
  Passes to technical conferences can help in these areas too.
  Shell accounts on remote computers can be used for compiling and 
  testing packages.
  </p>

  <p>
  When a developer makes a request, he or she must state the purpose and
  what project(s) benefit from it. So, a "needed resource" is anything that
  helps accomplish a project related task for a developer. A full system
  may be needed in some cases (example, for building releases and testing
  live CDs). Some architecture teams are seriously lacking the CPU power to
  build GRP releases in a reasonable amount of time.
  </p>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>Community: How to offer Resources</title>
  <section>
  <body>

  <p>
  If you wish to donate any of the resources listed in the <uri 
  link="#doc_chap7">Developers Seeking Resources</uri> section or if you 
  want to purchase 1 or more of those items and have them shipped to a 
  developer, then you should submit a completed <uri 
  link="./offer.txt">offer form</uri> to get put in touch with the 
  developer. Do not contact the developer directly, use 
  <mail link="adopt-a-dev@gentoo.org">adopt-a-dev@gentoo.org</mail>. We 
  like to keep track of things to ensure that our lists are as accurate 
  and as up to date as humanly possible.
  </p>

  <p>
  You can also offer up any existing resources you have that are 
  in working good condition and would enhance the development process.
  To be listed in the <uri link="#doc_chap8">Community Members 
  Offering Resources</uri> section, submit a completed <uri
  link="./offer.txt">offer form</uri> to 
  <mail link="adopt-a-dev@gentoo.org">adopt-a-dev@gentoo.org</mail>. If 
  you wish to remain anonymous, please let us know. More details on 
  privacy in the <uri link="#doc_chap10">Privacy Policy</uri>.
  </p>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>Developers: How to request Resources</title>
  <section>
  <body>

  <p>
  Submit a completed <uri link="./request.txt">request form</uri> to 
  <mail link="adopt-a-dev@gentoo.org">adopt-a-dev@gentoo.org</mail>, a 
  team member will review the request and post it to the site. The 
  review process is only meant to ensure that the item can be used for a 
  Gentoo project and to ensure that the request is clearly written. In 
  general, we will post all requests. If we find that your request needs 
  elucidation, then we will contact you. Your request must be sent from 
  your @gentoo.org e-mail address.
  </p>

  <p>
  You must be an official Gentoo developer for at least 6 months
  before requesting or receiving resources through adopt a developer.
  You are limited to 4 open requests at any given time. You may ask for any
  of your open requests to be changed or removed to make room for a new
  request. There is no hard limit on the amount of resources you receive.
  However, please use common sense. Only ask for 
  things you need and will use to improve Gentoo. Additionally, if you
  have gotten hardware through adopt-a-dev and you no longer use the
  hardware or have a need for it, please consider offering it to
  other developers.
  </p>

  <p>
  The adopt a developer project strongly encourages developers who get 
  resources through adopt a developer to write publicly (<uri 
  link="http://planet.gentoo.org">planet</uri>, <uri 
  link="http://forums.gentoo.org">forums</uri>, <uri
  link="http://www.gentoo.org/main/en/lists.xml">mailing lists</uri>, 
  etc) about any progress they have made with the help of the donated 
  resources, what goals they have accomplished, and how the resources 
  have helped them.
  </p>

  <p>
  In the event that a user offers a resource and 2 or more developers
  want the resource, then the adopt-a-dev team will work with the 
  developers involved to see if the resource can be shared. If its 
  something like a complete system, then maybe a shell account could be
  setup for the other dev(s) involved. If it is a resource that can't easily be 
  shared, then maybe the developers could swap the item after a certain 
  amount of time. If those methods fail, then the resource will go to the
  developer who requested it first. The one who requested it first is the
  developer whose completed request form arrived first.
  </p>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>Developers Seeking Resources</title>
  <section>
  <body>

  <table>
    <tr>
      <th>Developer</th>
      <th>Location</th>
      <th>Qnty</th>
      <th>Resource</th>
      <th>Purpose</th>
      <th>Project</th>
    </tr>
<?php
$result = db_query("SELECT first, last, location, quantity, resource, purpose, resourceid from people, resources, status where status.statusid = resources.status and status.status = 'seeking' and people.peopleid = devid");

while ($result->has_next()) {
  $row = $result->get_row();

  for ($i = 0; $i <= 5; $i++) {
    $uid = make_bbcode_uid();
    $row[$i] = bbencode_first_pass($row[$i],$uid);
    $row[$i] = bbencode_second_pass($row[$i],$uid);
  }

  echo "<tr>\n";
  echo "  <ti>$row[0] $row[1]</ti>\n";
  echo "  <ti>$row[2]</ti>\n";
  echo "  <ti>$row[3]</ti>\n";
  echo "  <ti>$row[4]</ti>\n";
  echo "  <ti>$row[5]</ti>\n";
  echo "  <ti>";
  $subresult = db_query("SELECT name,url from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$row[6]'");
  while ($subresult->has_next()) {
    $subrow = $subresult->get_row();
    echo "<uri link=\"$subrow[1]\">$subrow[0]</uri> ";
  }
  echo "</ti>\n";
  echo "</tr>\n";
}
?>
  </table>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>Community Members Offering Resources</title>
  <section>
  <body>

  <table>
    <tr>
      <th>Name</th>
      <th>Location</th>
      <th>Qnty</th>
      <th>Resource</th>
    </tr>
<?php
$result = db_query("SELECT first, last, location, quantity, resource from people, resources, status where status.statusid = resources.status and status.status = 'offering' and people.peopleid = donorid");
while ($result->has_next()) {
  $row = $result->get_row();

  for ($i = 0; $i <= 4; $i++) {
    $uid = make_bbcode_uid();
    $row[$i] = bbencode_first_pass($row[$i],$uid);
    $row[$i] = bbencode_second_pass($row[$i],$uid);
  }


  echo "<tr>\n";
  echo "  <ti>$row[0] $row[1]</ti>\n";
  echo "  <ti>$row[2]</ti>\n";
  echo "  <ti>$row[3]</ti>\n";
  echo "  <ti>$row[4]</ti>\n";
  echo "</tr>\n";
}
?>
  </table>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>Thank You</title>
  <section>
  <body>

  <p>
  The Gentoo project would very much like to thank the following people
  for their generosity.
  </p>

  <table>
    <tr>
      <th>Donor</th>
      <th>Developer</th>
      <th>Qnty</th>
      <th>Resource</th>
      <th>Purpose</th>
      <th>Project</th>
    </tr>
<?php
$result = db_query("SELECT donor.first, donor.last, dev.first, dev.last, quantity, resource, purpose, resourceid from people as donor, people as dev, resources, status where status.statusid = resources.status and status.status = 'thanks' and dev.peopleid = devid and donor.peopleid = donorid");
while ($result->has_next()) {
  $row = $result->get_row();


  for ($i = 0; $i <= 6; $i++) {
    $uid = make_bbcode_uid();
    $row[$i] = bbencode_first_pass($row[$i],$uid);
    $row[$i] = bbencode_second_pass($row[$i],$uid);
  }


  echo "<tr>\n";
  echo "  <ti>$row[0] $row[1]</ti>\n";
  echo "  <ti>$row[2] $row[3]</ti>\n";
  echo "  <ti>$row[4]</ti>\n";
  echo "  <ti>$row[5]</ti>\n";
  echo "  <ti>$row[6]</ti>\n";
  echo "  <ti>";
  $subresult = db_query("SELECT name,url from projects, project_specific_resources where projects.projectid = project_specific_resources.projectid and project_specific_resources.resourceid = '$row[7]'");
  while ($subresult->has_next()) {
    $subrow = $subresult->get_row();
    echo "<uri link=\"$subrow[1]\">$subrow[0]</uri> ";
  }
  echo "</ti>\n";
  echo "</tr>\n";
}
?>
  </table>

  </body>
  </section>
</extrachapter>

<extrachapter>
  <title>Privacy Policy</title>
  <section>
  <body>

  <p>
  We aim to balance the respect for the personal privacy of every donor
  and potential donor while recognizing their contribution. Donor and 
  potential donor e-mail addresses will never be posted on the project 
  page. By default, names will be posted on the project page in the
  <uri link="#doc_chap8">Community Members Offering Resources</uri>
  and <uri link="#doc_chap9">Thank You</uri> sections. If you wish to 
  remain anonymous, please let us know in your correspondence with us.
  </p>

  </body>
  </section>
</extrachapter>


<extrachapter>
  <title>Disclaimer</title>
  <section>
  <body>

  <p>
  This project is only responsible for connecting donors with individual
  developers. Our responsibilities stop there. We hope all
  items donated to Gentoo developers are put to good use improving
  Gentoo. We cannot guarantee that a developer will accomplish all of
  the goals related to the resource(s) he or she receives.
  The project does not deal with organizing shipping, nor do we
  deal with disputes between developers and donors.
  Gentoo is not a 501(c)(3) organization but is applying for 501(c)(6)
  status instead. Therefore, donations are not and will not be
  tax-deductible in the U.S. In other countries, the laws will differ --
  please check with your lawyer.
  </p>

  </body>
  </section>
</extrachapter>

</project>

<?php
}
return new return_result(true);
}}
register_action(new xml_action("xml",1));
?>
