<?php
# Adopt a Developer
#
# Copyright (C) 2004, 2006 Thomas Cort
# Copyright (C) 2004 Adam Beaumont, Thomas Cort, Patrick McLean, Scott Stoddard
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class default_action extends actor {
  function execute() {
    trigger("html_headers");
    trigger("default");
    trigger("html_footers");
    return new return_result(true);
  }
}

class default_event extends actor {
  function execute() {
    global $username, $accesslevel;
    trigger("begin_story");

    if ($username == "guest") {
?>
<p>
This web application is used by the Adopt a Developer team to manage project related information
such as resources requested by developers, resources offered by community members, previous 
donations, thanks yous, and contact information. Unless you are an authorized member of the
Adopt a Developer project and have a valid login, this page will not very useful to you.
</p>
<p>
If you would like infomation about the Adopt a Developer project, please 
visit our project page at the folling URL: <a 
href="http://www.gentoo.org/proj/en/userrel/adopt-a-dev">http://www.gentoo.org/proj/en/userrel/adopt-a-dev</a>
</p>
<?php } else { ?>
<h2>Welcome, <?php echo $username; ?>!</h2>
<p>
Please choose from one of the following tasks:
<ul>
<li><a href="./?a=resource">Add/Change Resources</a></li>
<li><a href="./?a=people">Add/Change People</a></li>
<li><a href="./?a=project">Add/Change Projects</a></li>
<li><a href="./?a=xml">Generate a new project page</a></li>
</ul>
</p>
<?php
    }

    trigger("end_story");
    return new return_result(true);
  }
}

register_handler(new default_event("default",50));
register_action(new default_action("default",50));

?>
