<?php
# Adopt a Developer
#
# Copyright (C) 2006 Thomas Cort
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

class people_action extends actor {
  function execute() {
    global $accesslevel, $username;
    trigger("html_headers");
    if (isset($_REQUEST['people'])) {
      trigger("people");
      trigger("display_people");
    } else if (isset($_REQUEST['people_add'])) {
      trigger("people_add");
      trigger("display_people");
    } else {
      trigger("display_people");
    }
    trigger("html_footers");
    return new return_result(true);
  }
}

class display_people_event extends actor {
  function execute() {

    trigger("begin_story");

    if ($username != "guest") {
      echo "<h2>People (in order by last name)</h2>";
      $result = db_query("select peopleid, first, last, email, location from people order by last");
      while ($result->has_next()) {
        $row = $result->get_row();
        echo "<form action=\"./\" method=\"post\"><table>";
        echo "<tr><th>First Name: </th><td bgcolor=\"#eeeeee\" colspan=\"2\">";
        echo "<input type=\"text\" name=\"first\" value=\"$row[1]\"></td>";
        echo "<th>Last Name: </th><td bgcolor=\"#eeeeee\" colspan=\"2\">";
        echo "<input type=\"text\" name=\"last\" value=\"$row[2]\"></td></tr>";
        echo "<tr><th>e-mail: </th><td bgcolor=\"#eeeeee\" colspan=\"2\">";
        echo "<input type=\"text\" name=\"email\" value=\"$row[3]\"></td>";
        echo "<th>Location: </th><td bgcolor=\"#eeeeee\" colspan=\"2\">";
        echo "<input type=\"text\" name=\"location\" value=\"$row[4]\"></td></tr><tr><td bgcolor=\"#eeeeee\">";
        echo "<input type=\"hidden\" name=\"a\" value=\"people\">";
        echo "<input type=\"hidden\" name=\"people\" value=\"1\">";
        echo "<input type=\"hidden\" name=\"peopleid\" value=\"$row[0]\">";
        echo "&nbsp;</td><td bgcolor=\"#eeeeee\"><input type=\"submit\" id=\"button\" value=\"change\">";
        echo "</td><td bgcolor=\"#eeeeee\">&nbsp;</td><td bgcolor=\"#eeeeee\">&nbsp;</td><td bgcolor=\"#eeeeee\"><input type=\"reset\" id=\"button\" value=\"clear\"></td></tr>";
        echo "</table></form><br>";
      }
      echo "<h2>Add a Person</h2>";
      echo "<form action=\"./\" method=\"post\"><table>";
      echo "<tr><th>First Name: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"text\" name=\"first\"></td></tr><tr><td>";
      echo "<tr><th>Last Name: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"text\" name=\"last\"></td></tr><tr><td bgcolor=\"#eeeeee\">";
      echo "<tr><th>e-mail: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"text\" name=\"email\"></td></tr><tr><td bgcolor=\"#eeeeee\">";
      echo "<tr><th>Location: </td><td bgcolor=\"#eeeeee\" colspan=\"2\">";
      echo "<input type=\"text\" name=\"location\"></td></tr><tr><td bgcolor=\"#eeeeee\">";
      echo "<input type=\"hidden\" name=\"a\" value=\"people\">";
      echo "<input type=\"hidden\" name=\"people_add\" value=\"1\">";
      echo "&nbsp;</td><td bgcolor=\"#eeeeee\"><input type=\"submit\" id=\"button\" value=\"add\">";
      echo "</td><td bgcolor=\"#eeeeee\"><input type=\"reset\" id=\"button\" value=\"clear\"></td></tr>";
      echo "</table></form>";

    } else {

      echo "<h2>You aren't Logged In!</h2>";
      echo "<h2>Thank You! Come again!</h2>";

    }

    trigger("end_story");
    return new return_result(true);
  }
}

class people_event extends actor {
  function execute() {

    $peopleid = isset($_REQUEST['peopleid']) && is_numeric($_REQUEST['peopleid']) ? $_REQUEST['peopleid'] : "";
    $first    = isset($_REQUEST['first'])    ? $_REQUEST['first']    : "";
    $last     = isset($_REQUEST['last'])     ? $_REQUEST['last']     : "";
    $email    = isset($_REQUEST['email'])    ? $_REQUEST['email']    : "";
    $location = isset($_REQUEST['location']) ? $_REQUEST['location'] : "";

    if (is_numeric($peopleid) && $first != "" && $last != "" && $email != "" && location != "") {
      $result = db_query("SELECT * from people where first = '".doslashes($first)."' and last = '".doslashes($last)."' and email = '".doslashes($email)."' and location = '".doslashes($location)."'");
      if (!$result->has_next()) {
        $result = db_query("SELECT * from people where peopleid = $peopleid");
        if ($result->has_next()) {
          if (db_exec("update people set first = '".doslashes($first)."', last = '".doslashes($last)."', email = '".doslashes($email)."', location = '".doslashes($location)."'  where peopleid = $peopleid")) {
             trigger("begin_story");
             echo "<h2>Done!</h2>";
             trigger("end_story");
          } else {
             trigger("begin_story");
             echo "<h2>DB Error!</h2>";
             trigger("end_story");
          }
        } else {
          trigger("begin_story");
          echo "<h2>people ID does not exist!</h2>";
          echo "<h2>Thank You! Come again!</h2>";
          trigger("end_story");
        }
      } else {
        trigger("begin_story");
        echo "<h2>Nothing Has Changed!</h2>";
        echo "<h2>Thank You! Come again!</h2>";
        trigger("end_story");
      }
    } else {
      trigger("begin_story");
      echo "<h2>Incomplete Form Data!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
    }

    echo "<br>";
    return new return_result(true);
  }
}

class people_add_event extends actor {
  function execute() {

    $first    = isset($_REQUEST['first'])    ? $_REQUEST['first']    : "";
    $last     = isset($_REQUEST['last'])     ? $_REQUEST['last']     : "";
    $email    = isset($_REQUEST['email'])    ? $_REQUEST['email']    : "";
    $location = isset($_REQUEST['location']) ? $_REQUEST['location'] : "";

    if ($first != "" && $last != "" && $email != "" && $location != "") {
       if (db_exec("insert into people (first,last,email,location) values ('".doslashes($first)."', '".doslashes($last)."', '".doslashes($email)."', '".doslashes($location)."')")) {
         trigger("begin_story");
         echo "<h2>Done!</h2>";
          trigger("end_story");
       } else {
         trigger("begin_story");
         echo "<h2>DB Error!</h2>";
         trigger("end_story");
      }
    } else {
      trigger("begin_story");
      echo "<h2>Incomplete Form Data!</h2>";
      echo "<h2>Thank You! Come again!</h2>";
      trigger("end_story");
    }

    echo "<br>";
    return new return_result(true);
  }
}

register_handler(new people_event("people",50));
register_handler(new people_add_event("people_add",50));
register_handler(new display_people_event("display_people",50));
register_action(new people_action("people",50));
?>
